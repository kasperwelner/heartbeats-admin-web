import Vapor
import HTTP

/// Here we have a controller that helps facilitate
/// creating typical REST patterns
final class UserController: ResourceRepresentable {
    let view: ViewRenderer
    
    init(_ view: ViewRenderer) {
        self.view = view
    }
    
    /// GET /hello
    func index(_ req: Request) throws -> ResponseRepresentable {
        return try view.make("users", for: req)
    }
    
    /// GET /hello/:id
    func show(_ req: Request, id:Int) throws -> ResponseRepresentable {
        return try view.make("users", for: req)
    }
    
    /// When making a controller, it is pretty flexible in that it
    /// only expects closures, this is useful for advanced scenarios, but
    /// most of the time, it should look almost identical to this
    /// implementation
    func makeResource() -> Resource<Int> {
        return Resource(
            index: index,
            show: show
        )
    }
}
