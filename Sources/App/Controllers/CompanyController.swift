//
//  CompanyController.swift
//  heartbeats-admin-webPackageDescription
//
//  Created by Kasper Welner on 12/12/2017.
//

import Foundation
import Vapor
import HTTP

/// Here we have a controller that helps facilitate
/// creating typical REST patterns
final class CompanyController: ResourceRepresentable {
    let view: ViewRenderer
    
    init(_ view: ViewRenderer) {
        self.view = view
    }
    
    /// GET /companies
    func index(_ req: Request) throws -> ResponseRepresentable {
        return try view.make("companies", for: req)
    }
    
    /// GET /companies/:id
    func show(_ req: Request, id:Int) throws -> ResponseRepresentable {
        return try view.make("companies", for: req)
    }
    
    /// When making a controller, it is pretty flexible in that it
    /// only expects closures, this is useful for advanced scenarios, but
    /// most of the time, it should look almost identical to this
    /// implementation
    func makeResource() -> Resource<Int> {
        return Resource(
            index: index,
            show: show
        )
    }
}
