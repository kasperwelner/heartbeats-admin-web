//
//  LoginController.swift
//  heartbeats-admin-webPackageDescription
//
//  Created by Kasper Welner on 05/11/2017.
//

import Vapor
import HTTP
import AuthProvider

final class LoginController: ResourceRepresentable {
    let view: ViewRenderer
    
    init(_ view: ViewRenderer) {
        self.view = view
    }
    
    /// GET /login
    func index(_ req: Request) throws -> ResponseRepresentable {
        return try view.make("login", for: req)
    }
    
    func makeResource() -> Resource<Int> {
        return Resource(
            index: index
        )
    }
}
