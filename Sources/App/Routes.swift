import Vapor
import AuthProvider

final class Routes: RouteCollection {
    let view: ViewRenderer
    init(_ view: ViewRenderer) {
        self.view = view
    }

    func build(_ builder: RouteBuilder) throws {

        /// GET /users/...
        builder.resource("users", UserController(view))

        /// GET /companies/...
        builder.resource("companies", CompanyController(view))

        /// GET /employees/...
        builder.resource("employees", EmployeeController(view))
        
        /// GET /login
        builder.resource("login", LoginController(view))
        
        // response to requests to /info domain
        // with a description of the request
        builder.get("info") { req in
            return req.description
        }
        
        builder.get("") { req in
            return Response(redirect: "/employees")
        }
    }
}

