/*jshint esversion: 6 */
var allUsers;

$(function() {
    let hasToken = Token.check();
    if (!hasToken) {
        window.location.href = "login";
        return;
    }
    let notification = $("#notification");
    notification.find("button").click(() => notification.hide(200));
    notification.hide();
    getUsers(getUsersSuccessCallback, getUsersErrorCallback);
    NavBar.configure(Item.EMPLOYEES);
    bindAddButton();
    setupNewEmployeeForm();
})

function bindRowButtons() {
    $("tr.user").each(function(index) {
        var userId = $(this).attr("userId");
        var firstName = $(this).attr("firstName");
        var lastName = $(this).attr("lastName");
        $(this).find("#delete-button").click(function() {
            showDeleteConfirmation(userId, firstName+" "+lastName);
        });
        $(this).find("#edit-button").click(function() {
            let user = allUsers[index];
            showEditFormFor(user);
        });
    });
}

function bindAddButton() {
    $("#addEmployeeButton").click(function() {
        showNewEmployeeForm();
    });
}


function getUsers(successCallback, errorCallback) {
    console.log(successCallback);

    var slug = '/api/employees';

    $.ajax({
        url: config.baseURL + slug,
        type: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + Token.get());
        },
        success: successCallback,
        error: errorCallback
    });
}

function getUsersSuccessCallback(response) {
    allUsers = response;
    populate(response);
    bindRowButtons();
}

function getUsersErrorCallback(jqXHR, textStatus, errorThrown) {
    let notification = $("#notification");
    notification.children(".text").text(errorThrown);
    notification.addClass("is-danger");
    notification.show();
}

function deleteUser(id, successCallback, errorCallback) {
    var slug = '/api/employees/' + id;
    $.ajax({
        url: config.baseURL + slug,
        type: 'DELETE',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + Token.get());
        },
        success: successCallback,
        error: errorCallback
    });
}

function deleteUserSuccessCallback(userId) {
    let element = $(`tr.user[userId=${userId}]`);
    console.log(element);
    element.fadeOut();
    let notification = $("#notification");
    notification.children(".text").text("Deleted!");
    notification.addClass("is-warning");
    notification.show();
}

function deleteErrorCallback(jqXHR, textStatus, errorThrown) {
    let notification = $("#notification");
    notification.children(".text").text(errorThrown + ": " + jqXHR.responseJSON.debugReason);
    notification.addClass("is-danger");
    notification.show();
}

function populate(users) {
    let me = localStorage.getItem("me");
    let isAuthedUserAdmin = JSON.parse(me).accessLevel == 'sysadmin';
    if (!isAuthedUserAdmin) {
        $('#companyIdHeader').hide();
    }

    jQuery.each(users, function(i, user) {
        let html =
            `<tr class="user" userId="${user.id}" firstName="${user.firstName}" lastName="${user.lastName}">
            <th>
                ${user.id}
            </th>
            <td style='text-transform:capitalize'>
                ${user.firstName}
            </td>
            <td style='text-transform:capitalize'>
                ${user.lastName}
            </td>
            <td ${isAuthedUserAdmin ? '' : "class=\'is-hidden\'"}>
                ${user.companyId || 'None'}
            </td>
            <td>
                <button class="button is-warning" id="edit-button">
                    Edit
                </button>
            </td>
            <td>
                <button
                    type="button"
                    class="button is-danger"
                    id="delete-button">
                    <span>
                        Delete
                    </span>
                    <span class="icon is-small">
                        <i class="fa fa-times"></i>
                    </span>
                </button>
            </td>
        </tr>
    `
        $("#userstbody").append(html);

    });
}

function showDeleteConfirmation(userId, name) {

    let deleteTitle = $('.modal-card-title');
    deleteTitle.text('Really delete ' + name + "?");

    let deleteCancelButton = $('#cancel-button');
    deleteCancelButton.off('click');
    deleteCancelButton.click(hideDeleteConfirmation);

    let deleteButton = $('#delete-confirmation-button');
    deleteButton.off('click');
    deleteButton.click(function() {
        hideDeleteConfirmation();
        deleteUser(userId,
            () => deleteUserSuccessCallback(userId),
            deleteErrorCallback);
    });
    $('#delete-confirmation').addClass("is-active");
}

function hideDeleteConfirmation() {
    $('#delete-confirmation').removeClass("is-active");
}

function setupNewEmployeeForm() {
    let container = $('#new-employee-form');
    let inputForm = $('#input-form');
    
    $("#cancel-button").click(() => { container.removeClass("is-active"); } );
    $("#submit-button").click(() => { 
        console.log('click!');
        console.log(inputForm);
        inputForm.submit();
    });
    inputForm.submit(attemptCreate);

    $("#pension-selector-yes").click(() => { 
        console.log('enable!');    
        inputForm.find(".pensioninput").prop("disabled", false);
    });
    
    $("#pension-selector-no").click(() => { 
        console.log('disable!');
        inputForm.find(".pensioninput").prop("disabled", true);
    });

    $('[data-toggle="datepicker"]').datepicker();

    $(".input").bind("input", updateSubmitButtonState);
    // $("#last-name-input").addEventListener("input", fieldChangeHandler);
    // $("#position-input").change( fieldChangeHandler );
}

function updateSubmitButtonState() {
    let state = submitButtonState();
    $("#submit-button").prop("disabled", !state);
}

function showNewEmployeeForm() {
    let container = $('#new-employee-form');
    container.removeClass('is-editing');
    container.find(".modal-card-title").text("Create new Employee");
    $("#submit-button").text("Create");
    container.addClass("is-active");
    $('#input-form').attr("userId", "");
    $('#companyp-id-field').prop("disabled", false);
    populateFields([]);
    updateSubmitButtonState();
}

function showEditFormFor(user) {
    populateFields(user);
    $('#input-form').attr("userId", user.id);
    let container = $('#new-employee-form');
    container.addClass('is-editing');
    container.find(".modal-card-title").text("Edit Employee");
    $("#submit-button").text("Update");
    container.addClass("is-active");
    $('#company-id-field').prop("disabled", true);
    updateSubmitButtonState();
}

function attemptCreate(event) {
    let editing = $('#new-employee-form').hasClass('is-editing');

    let inputForm = $('#input-form');
    let userId = inputForm.attr("userId");

    let me = localStorage.getItem("me");
    let isAuthedUserAdmin = JSON.parse(me).accessLevel == 'sysadmin';

    var data = inputForm.serializeArray();
    console.log(data);
    var dataDict = getFormData(data);
    if ( !isAuthedUserAdmin ) {
        let myCompanyId = JSON.parse(me).companyId;
        dataDict.companyId = myCompanyId;
    }
    if ( ('pensionStartDate' in dataDict) == false ) {
        let date = new Date();
        dataDict.pensionStartDate = date.toISOString();
    } else {
        dataDict.pensionStartDate = $('[data-toggle="datepicker"]').datepicker('getDate').toISOString();
    }
    if ( ('pensionCustomPercent' in dataDict) == false ) {
        dataDict.pensionCustomPercent = 0;
    }

    let jsonData = JSON.stringify(dataDict);
    console.log(jsonData);

    var method = editing ? 'PATCH' : 'POST';

    var url = config.baseURL + '/api/employees/';
    if (editing) {
        url = url+userId;
    }

    let options = {
        url: url,
        type:      method,
        dataType:  'json',
        data:      jsonData, 
        success:   postEmployeeSuccessCallback,
        error:     postEmployeeErrorCallback,
        beforeSerialize: function($form, options) {        
            console.log(options);
            return true;
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + Token.get());
            xhr.setRequestHeader("Content-Type", "application/json");
        },        
    };
    
    $.ajax(options);

    return false;
}

function getFormData(array) {
    var indexed_array = {};

    $.map(array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

function populateFields(dict) {
    $('#first-name-input').val(dict.firstName);
    $('#last-name-input').val(dict.lastName);
    $('#position-input').val(dict.title);
    $('#salary-input').val(dict.monthlySalary);
    $('#hours-input').val(dict.hoursPerWeek);
    $("#pension-selector-yes").prop('checked', dict.hasPension);
    $("#pension-selector-no").prop('checked', !dict.hasPension);
    $('#input-form').find(".pensioninput").prop("disabled", !dict.hasPension);
    if (dict.hasPension) {
        $('[data-toggle="datepicker"]').datepicker('setDate', new Date(dict.pensionStartDate));
    } else {
        $('#pension-start-date-field').val("");    
    }
    $('#pension-percent-field').val(dict.pensionCustomPercent);

    let me = localStorage.getItem("me");
    let isAuthedUserAdmin = JSON.parse(me).accessLevel == 'sysadmin';
    if (isAuthedUserAdmin) {
        $('#company-id-field-container').show();
        $('#company-id-field').val(dict.companyId);
    } else {
        $('#company-id-field-container').hide();
    }
}

function postEmployeeSuccessCallback(response) {
    console.log(response);
    let container = $('#new-employee-form');
    container.removeClass("is-active");
    location.reload();
}

function postEmployeeErrorCallback(jqXHR, textStatus, errorThrown) {
    let container = $('#new-employee-form');
    container.removeClass("is-active");

    let notification = $("#notification");
    notification.children(".text").text(errorThrown + ": " + jqXHR.responseJSON.debugReason);
    notification.addClass("is-danger");
    notification.show();
}

function submitButtonState() {
    let inputForm = $('#input-form');
    var dataRaw = inputForm.serializeArray();
    var data = getFormData(dataRaw);
    console.log(data);

    if ( data.firstName.length == 0 ) {
        return false;
    }

    if ( data.lastName.length == 0 ) {
        return false;
    }

    if ( data.title.length == 0 ) {
        return false;
    }

    if ( data.hasPension == "true" && (data.pensionCustomPercent < 1 || data.pensionCustomPercent > 100) ) {
        return false;
    }

    let me = localStorage.getItem("me");
    let isAuthedUserAdmin = JSON.parse(me).accessLevel == 'sysadmin';
    if ( isAuthedUserAdmin && data.companyId.length == 0 ) {
        return false;
    }

    return true;
}