function bind() {
    bindDeleteButtons()
    console.log("HERE!")
    configureNotification()
}

function configureNotification() {
    let notification = $("#notification")
        //notification.hide()
    let me = localStorage.getItem("me")
    notification.children(".text").text(me)
    notification.find("button").click(() => notification.hide(200))
}

function bindDeleteButtons() {
    $("tr.user").each(function() {
        var userId = $(this).attr("userId")
        console.log(userId)
        $(this).find("button").click(function() {
            deleteUser(userId,
                () => deleteUserSuccessCallback(userId),
                deleteErrorCallback)
        })
    })
}

function deleteUser(id, successCallback, errorCallback) {
    console.log(`deleteUser! ${id}`)
    var slug = '/hello/' + id;
    console.log(id)

    $.ajax({
        url: slug,
        type: 'DELETE',
        success: successCallback,
        error: errorCallback
    });
}

function deleteUserSuccessCallback(userId) {
    let element = $(`tr.user[userId=${userId}]`)
    console.log(element);
    element.fadeOut()
    let notification = $("#notification")
    notification.children(".text").text("Deleted!");
    notification.addClass("is-warning")
    notification.show()
}

function deleteErrorCallback(jqXHR, textStatus, errorThrown) {
    let notification = $("#notification")
    notification.children(".text").text(errorThrown);
    notification.addClass("is-danger")
    notification.show()
}


var genericSuccessCallback = (data, result, jqXHR) => {}
var genericErrorCallback = (jqXHR, textStatus, errorThrown) => {}