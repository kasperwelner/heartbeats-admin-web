/*jshint esversion: 6 */

$(function() {
    let hasToken = Token.check()
    if (!hasToken) {
        window.location.href = "login"
        return;
    }
    let notification = $("#notification")
    notification.find("button").click(() => notification.hide(200))
    notification.hide();
    getCompanies(getCompaniesSuccessCallback, getCompaniesErrorCallback)
    NavBar.configure(Item.COMPANY);
})

function bindDeleteButtons() {
    $("tr.company").each(function() {
        var id = $(this).attr("companyId");
        var name = $(this).attr("name");
        $(this).find("button").click(function() {
            showDeleteConfirmation(id, name);
        });
    });
}

function getCompanies(successCallback, errorCallback) {
    console.log(successCallback);
    let me = localStorage.getItem("me");
    let isAuthedUserOwner = JSON.parse(me).accessLevel == 'sysadmin';

    var slug;
    if (isAuthedUserOwner) {
        slug = '/api/companies';
    } else {
        slug = '/api/companies/'+JSON.parse(me).companyId;
    }

    $.ajax({
        url: config.baseURL + slug,
        type: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + Token.get());
        },
        success: successCallback,
        error: errorCallback
    });
}

function getCompaniesSuccessCallback(response) {
    let me = localStorage.getItem("me");
    let isAuthedUserOwner = JSON.parse(me).accessLevel == 'sysadmin';
    if (isAuthedUserOwner) {
        populate(response);
    } else {
        populate([response]);
    }
    
    bindDeleteButtons();
}

function getCompaniesErrorCallback(jqXHR, textStatus, errorThrown) {
    let notification = $("#notification")
    notification.children(".text").text(errorThrown);
    notification.addClass("is-danger")
    notification.show()
}

function deleteCompany(id, successCallback, errorCallback) {
    var slug = '/api/companies/' + id;
    $.ajax({
        url: config.baseURL + slug,
        type: 'DELETE',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + Token.get());
        },
        success: successCallback,
        error: errorCallback
    });
}

function deleteCompanySuccessCallback(id) {
    let element = $(`tr.user[id=${id}]`)
    console.log(element);
    element.fadeOut();
    let notification = $("#notification");
    notification.children(".text").text("Deleted!");
    notification.addClass("is-warning");
    notification.show();
}

function deleteErrorCallback(jqXHR, textStatus, errorThrown) {
    let notification = $("#notification")
    notification.children(".text").text(errorThrown + ": " + jqXHR.responseJSON.debugReason);
    notification.addClass("is-danger");
    notification.show();
}

function populate(users) {
    let me = localStorage.getItem("me");
    let isAuthedUserOwner = JSON.parse(me.accessLevel == 'sysadmin');

    jQuery.each(users, function(i, company) {
        let html =
            `<tr class="company" companyId="${company.id}" name="${company.name}">
            <th>
                ${company.id}
            </th>
            <th>
                ${company.name}
            </th>
            <td>
                ${company.userInvitationCode}
            </td>
            <td>
                ${company.ownerInvitationCode}
            </td>
            <td>
                <button
                    type="button"
                    class="button is-danger">
                    <span>
                        Delete
                    </span>
                    <span class="icon is-small">
                        <i class="fa fa-times"></i>
                    </span>
                </button>
            </td>
        </tr>
    `
        $("#companiestbody").append(html);

    });

}

function showDeleteConfirmation(id, name) {

    let deleteTitle = $('.modal-card-title');
    deleteTitle.text('Really delete \"' + name + "\"?");

    let deleteCancelButton = $('#cancel-button');
    deleteCancelButton.off('click');
    deleteCancelButton.click(hideDeleteConfirmation);

    let deleteButton = $('#delete-confirmation-button');
    deleteButton.off('click');
    deleteButton.click(function() {
        hideDeleteConfirmation();
        deleteCompany(id,
            () => deleteCompanySuccessCallback(id),
            deleteErrorCallback);
    });
    $('.modal').addClass("is-active");
}

function hideDeleteConfirmation() {
    $('.modal').removeClass("is-active");
}