/*jshint esversion: 6 */
function bind() {
    $.getScript("JS/config.js", function() {
        bindLoginButton();
    });
}

function bindLoginButton() {
    $("#loginbutton").click(function() {
        $(this).addClass("is-loading")
        $("#errorlabel").hide()
        loginUser($("#emailfield").val(),
            $("#passwordfield").val(),
            loginUserSuccessCallback,
            loginErrorCallback)
    });
}

function loginUser(email, password, successCallback, errorCallback) {
    console.log(email);
    console.log(password);
    console.log(successCallback);

    var slug = '/api/login';


    $.ajax({
        url: config.baseURL + slug,
        type: 'POST',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + btoa(email + ":" + password));
        },
        success: successCallback,
        error: errorCallback
    });
}

function loginUserSuccessCallback(response) {
    console.log(response);
    let token = response.token;
    let me = response.user;
    Token.set(token);
    localStorage.setItem("me", JSON.stringify(me));
    $("#loginbutton").removeClass("is-loading");
    window.location.href = "users";
}

function loginErrorCallback(jqXHR, textStatus, errorThrown) {
    $("#loginbutton").removeClass("is-loading")
    let errorLabel = $("#errorlabel")
    errorLabel.show()
    let errorText = errorThrown
    if (!errorText) {
        errorText = "An unknown error occured"
    }
    errorLabel.text(errorText)
}

var genericSuccessCallback = (data, result, jqXHR) => {}
var genericErrorCallback = (jqXHR, textStatus, errorThrown) => {}
