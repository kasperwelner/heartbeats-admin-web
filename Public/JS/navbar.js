/*jshint esversion: 6 */
/*jshint strict:false */

const Item = Object.freeze({
    COMPANY: Symbol("company"),
    USERS: Symbol("users"),
    EMPLOYEES: Symbol("employees")
});

let NavBar = {
    configure: function(selectedItem) {
        'use strict';
        let me = localStorage.getItem("me");
        $("#nav-user-button").text(JSON.parse(me).name);
        $("#logout-button").click(function() {
            Token.delete();
            window.location.href = "login";
        })
        switch (selectedItem) {
            case Item.COMPANY:
                $("#company-item").addClass("is-active");
                break;
            case Item.USERS:
                $("#users-item").addClass("is-active")
                break;
            case Item.EMPLOYEES:
                $("#employees-item").addClass("is-active")       
                break;
        }
    }
}
