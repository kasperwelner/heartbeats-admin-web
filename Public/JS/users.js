/*jshint esversion: 6 */

$(function() {
    let hasToken = Token.check();
    if (!hasToken) {
        window.location.href = "login";
        return;
    }
    let notification = $("#notification")
    notification.find("button").click(() => notification.hide(200));
    notification.hide()
    getUsers(getUsersSuccessCallback, getUsersErrorCallback);
    NavBar.configure(Item.USERS);
})

function bindDeleteButtons() {
    $("tr.user").each(function() {
        var userId = $(this).attr("userId");
        var name = $(this).attr("username");
        $(this).find("button").click(function() {
            showDeleteConfirmation(userId, name);
        })
    })
}

function getUsers(successCallback, errorCallback) {
    console.log(successCallback);

    var slug = '/api/users';

    $.ajax({
        url: config.baseURL + slug,
        type: 'GET',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + Token.get());
        },
        success: successCallback,
        error: errorCallback
    });
}

function getUsersSuccessCallback(response) {
    populate(response);
    bindDeleteButtons();
}

function getUsersErrorCallback(jqXHR, textStatus, errorThrown) {
    let notification = $("#notification");
    notification.children(".text").text(errorThrown);
    notification.addClass("is-danger");
    notification.show();
}

function deleteUser(id, successCallback, errorCallback) {
    var slug = '/api/users/' + id;
    $.ajax({
        url: config.baseURL + slug,
        type: 'DELETE',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + Token.get());
        },
        success: successCallback,
        error: errorCallback
    });
}

function deleteUserSuccessCallback(userId) {
    let element = $(`tr.user[userId=${userId}]`);
    console.log(element);
    element.fadeOut();
    let notification = $("#notification");
    notification.children(".text").text("Deleted!");
    notification.addClass("is-warning");
    notification.show();
}

function deleteErrorCallback(jqXHR, textStatus, errorThrown) {
    let notification = $("#notification");
    notification.children(".text").text(errorThrown + ": " + jqXHR.responseJSON.debugReason);
    notification.addClass("is-danger");
    notification.show();
}

function populate(users) {
    let me = localStorage.getItem("me");
    let isAuthedUserOwner = JSON.parse(me.accessLevel == 'sysadmin');

    jQuery.each(users, function(i, user) {
        let html =
            `<tr class="user" userId="${user.id}" username="${user.name}">
            <th>
                ${user.id}
            </th>
            <th>
                ${user.name}
            </th>
            <td style='text-transform:lowercase'>
                ${user.email}
            </td>
            <td style='text-transform:capitalize'>
                ${user.accessLevel}
            </td>
            <td ${!isAuthedUserOwner ? '' : "class=\'is-hidden\'"}>
                ${user.companyId || 'None'}
            </td>
            <td>
                <button
                    type="button"
                    class="button is-danger">
                    <span>
                        Delete
                    </span>
                    <span class="icon is-small">
                        <i class="fa fa-times"></i>
                    </span>
                </button>
            </td>
        </tr>
    `
        $("#userstbody").append(html);

    });

    if (!isAuthedUserOwner) {
        $("#company-id").addClass("is-hidden")
    }

}

function showDeleteConfirmation(userId, name) {

    let deleteTitle = $('.modal-card-title');
    deleteTitle.text('Really delete \"' + name + "\"?");

    let deleteCancelButton = $('#cancel-button');
    deleteCancelButton.off('click');
    deleteCancelButton.click(hideDeleteConfirmation);

    let deleteButton = $('#delete-confirmation-button');
    deleteButton.off('click');
    deleteButton.click(function() {
        hideDeleteConfirmation();
        deleteUser(userId,
            () => deleteUserSuccessCallback(userId),
            deleteErrorCallback);
    });
    $('.modal').addClass("is-active");
}

function hideDeleteConfirmation() {
    $('.modal').removeClass("is-active");
}
