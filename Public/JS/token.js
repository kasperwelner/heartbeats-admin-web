Token = {
    check: function() {
        if (!Token.get()) {
            return false;
        }
        return true;
    },

    get: function() {
        return Cookies.get('token');
    },

    set: function(token) {
        Cookies.set('token', token);
    },

    delete: function() {
        Cookies.remove('token');
    }
}